#!/bin/python
from subprocess import Popen
from sys import argv
import shutil
import os
if __name__ == "__main__":
    user = argv[1]
    pswd = argv[2]
    with open('modules.txt') as f:
        modules = f.read().split('\n')

    for module in modules:
        suffix = module.split('@')[-1]
        module_name = suffix.split("/")[-1].split('.git')[0]
        full_url = "https://{}:{}@{}".format(user, pswd, suffix)
        shutil.rmtree(module_name, ignore_errors=True)
        try:
            os.remove(".gitignore")
        except:
            pass

        Popen("git clone {}".format(full_url).split(" ")).wait()
        try:
            shutil.rmtree("{}/.git".format(module_name), ignore_errors=True)
            os.remove("{}/.gitignore".format(module_name))
        except:
            pass
